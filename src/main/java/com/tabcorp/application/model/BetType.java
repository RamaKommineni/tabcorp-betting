package com.tabcorp.application.model;

public enum BetType {
	WIN, PLACE, TRIFECTA, DOUBLE, QUADDIE;

}
